%global debug_package %{nil}
%define scons_package scons
%define scons scons
%define distribution %(/usr/lib/rpm/redhat/dist.sh --distnum)
%if 0%{distribution} >= 8
%define scons_package python3-scons
%define scons scons-3
%endif

%define boost_ver 1_71_0
%define boost_ver_dots 1.71.0

Name:           eos-folly-deps
Summary:        Boost library, packaged as EOS dependency
Version:        2019.11.11.00
Release:        1%{dist}%{?_with_asan:.asan}%{?_with_tsan:.tsan}
License:        Apache
URL:            https://github.com/boostorg/boost
Source0:        https://boostorg.jfrog.io/artifactory/main/release/%{boost_ver_dots}/source/boost_%{boost_ver}.tar.gz
Source1:        https://github.com/google/glog/archive/v0.4.0.tar.gz
Source2:        https://github.com/gflags/gflags/archive/v2.2.2.tar.gz
Source3:        https://github.com/google/double-conversion/archive/v1.1.6.tar.gz
Source4:        SConstruct.double-conversion
%if 0%{?rhel} == 9 || 0%{?fedora} >= 35
Patch0:		boost.patch
%endif

# For asan/tsan builds we need devtoolset-9
%if %{?_with_asan:1}%{!?_with_asan:0} || %{?_with_tsan:1}%{!?_with_tsan:0}
%define devtoolset devtoolset-9
%else
%define devtoolset devtoolset-8
%endif

%if 0%{?rhel} == 7
BuildRequires: %{devtoolset}-gcc-c++
BuildRequires: cmake3
%define cmake cmake3
%else
BuildRequires: perl-Data-Dumper
BuildRequires: cmake
%define cmake cmake
%endif

BuildRequires: gcc-c++
BuildRequires: make
BuildRequires: which
BuildRequires: zlib-static
BuildRequires: zlib-devel
BuildRequires: m4
BuildRequires: automake
BuildRequires: libtool
BuildRequires: %{scons_package}
BuildRequires: openssl
BuildRequires: openssl-devel
BuildRequires: libevent
BuildRequires: libevent-devel

%if %{?_with_asan:1}%{!?_with_asan:0}
%if 0%{?rhel} == 7
BuildRequires: libasan5, %{devtoolset}-libasan-devel
Requires: libasan5
%else
BuildRequires: libasan
Requires: libasan
%endif
%endif

%if %{?_with_tsan:1}%{!?_with_tsan:0}
%if 0%{?rhel} == 7
BuildRequires: libtsan, %{devtoolset}-libtsan-devel
Requires: libtsan
%else
BuildRequires: libtsan
Requires: libtsan
%endif
%endif

%description
Boost used as EOS build dependency.

%package devel
Summary: eos-folly-deps development files
Group: Development/Libraries

%description devel
This package provides headers and libraries for eos-folly-deps.

%prep
%setup -q -c -n eos-folly-deps -a 0 -a 1 -a 2 -a 3
%if 0%{?rhel} == 9 || 0%{?fedora} >= 35
%patch -P 0 -p1
%endif

%build
%if %{?_with_asan:1}%{!?_with_asan:0}
export CXXFLAGS='-fsanitize=address'
%endif

%if %{?_with_tsan:1}%{!?_with_tsan:0}
export CXXFLAGS='-fsanitize=thread'
%endif

export CXXFLAGS="${CXXFLAGS} -g3 -fPIC"

%if 0%{?rhel} == 7
source /opt/rh/%{devtoolset}/enable
%endif

%if 0%{?fedora} != 0
export CXXFLAGS="${CXXFLAGS} -Wno-deprecated-declarations"
%endif

mkdir TEMPROOT
TEMP_ROOT=$PWD/TEMPROOT

#-------------------------------------------------------------------------------
# Compile boost
#-------------------------------------------------------------------------------
pushd boost_%{boost_ver}
./bootstrap.sh --prefix=${RPM_BUILD_ROOT}/opt/eos-folly --with-libraries=context,thread,program_options,regex,system,chrono,filesystem,date_time
./b2 %{?_smp_mflags}
popd

#-------------------------------------------------------------------------------
# Compile gflags
#-------------------------------------------------------------------------------
pushd gflags-2.2.2
mkdir build && cd build
%{cmake} -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=/opt/eos-folly -DCMAKE_INSTALL_LIBDIR=lib ..
make %{?_smp_mflags}
popd

#-------------------------------------------------------------------------------
# Compile glog
#-------------------------------------------------------------------------------
pushd glog-0.4.0
mkdir build && cd build
%{cmake} -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=/opt/eos-folly -DCMAKE_INSTALL_LIBDIR=lib ..
make %{?_smp_mflags}
popd

#-------------------------------------------------------------------------------
# Compile double-conversion
#-------------------------------------------------------------------------------
pushd double-conversion-1.1.6
%{scons} -f %{SOURCE4}
%{__install} -D -m 755 ./libdouble_conversion.a ${TEMP_ROOT}/lib/libdouble-conversion.a
%{__install} -D -m 755 ./libdouble_conversion_pic.a ${TEMP_ROOT}/lib/libdouble-conversion_pic.a
%{__install} -D -m 755 ./src/double-conversion.h ${TEMP_ROOT}/include/double-conversion/double-conversion.h
%{__install} -D -m 755 ./src/bignum.h  ${TEMP_ROOT}/include/double-conversion/bignum.h
%{__install} -D -m 755 ./src/bignum-dtoa.h ${TEMP_ROOT}/include/double-conversion/bignum-dtoa.h
%{__install} -D -m 755 ./src/cached-powers.h ${TEMP_ROOT}/include/double-conversion/cached-powers.h
%{__install} -D -m 755 ./src/diy-fp.h ${TEMP_ROOT}/include/double-conversion/diy-fp.h
%{__install} -D -m 755 ./src/fast-dtoa.h ${TEMP_ROOT}/include/double-conversion/fast-dtoa.h
%{__install} -D -m 755 ./src/fixed-dtoa.h ${TEMP_ROOT}/include/double-conversion/fixed-dtoa.h
%{__install} -D -m 755 ./src/ieee.h ${TEMP_ROOT}/include/double-conversion/ieee.h
%{__install} -D -m 755 ./src/strtod.h ${TEMP_ROOT}/include/double-conversion/strtod.h
%{__install} -D -m 755 ./src/utils.h ${TEMP_ROOT}/include/double-conversion/utils.h
popd

%install

%if 0%{?rhel} == 7
source /opt/rh/%{devtoolset}/enable
%endif

#-------------------------------------------------------------------------------
# Install boost
#-------------------------------------------------------------------------------
pushd boost_%{boost_ver}
./b2 install %{?_smp_mflags}
popd

#-------------------------------------------------------------------------------
# Install gflags
#-------------------------------------------------------------------------------
pushd gflags-2.2.2
cd build
make DESTDIR=%{buildroot} install %{?_smp_mflags}
popd

#-------------------------------------------------------------------------------
# Install glog
#-------------------------------------------------------------------------------
pushd glog-0.4.0
cd build
make DESTDIR=%{buildroot} install %{?_smp_mflags}
popd

#-------------------------------------------------------------------------------
# Clean up /root/.cmake
#-------------------------------------------------------------------------------
rm -rf %{buildroot}/root/

#-------------------------------------------------------------------------------
# Install double-conversion
#-------------------------------------------------------------------------------
pushd double-conversion-1.1.6
%{__install} -D -m 755 ./libdouble_conversion_pic.a ${RPM_BUILD_ROOT}/opt/eos-folly/lib/libdouble-conversion.a
%{__install} -D -m 755 ./src/double-conversion.h ${RPM_BUILD_ROOT}/opt/eos-folly/include/double-conversion/double-conversion.h
%{__install} -D -m 755 ./src/bignum.h  ${RPM_BUILD_ROOT}/opt/eos-folly/include/double-conversion/bignum.h
%{__install} -D -m 755 ./src/bignum-dtoa.h ${RPM_BUILD_ROOT}/opt/eos-folly/include/double-conversion/bignum-dtoa.h
%{__install} -D -m 755 ./src/cached-powers.h ${RPM_BUILD_ROOT}/opt/eos-folly/include/double-conversion/cached-powers.h
%{__install} -D -m 755 ./src/diy-fp.h ${RPM_BUILD_ROOT}/opt/eos-folly/include/double-conversion/diy-fp.h
%{__install} -D -m 755 ./src/fast-dtoa.h ${RPM_BUILD_ROOT}/opt/eos-folly/include/double-conversion/fast-dtoa.h
%{__install} -D -m 755 ./src/fixed-dtoa.h ${RPM_BUILD_ROOT}/opt/eos-folly/include/double-conversion/fixed-dtoa.h
%{__install} -D -m 755 ./src/ieee.h ${RPM_BUILD_ROOT}/opt/eos-folly/include/double-conversion/ieee.h
%{__install} -D -m 755 ./src/strtod.h ${RPM_BUILD_ROOT}/opt/eos-folly/include/double-conversion/strtod.h
%{__install} -D -m 755 ./src/utils.h ${RPM_BUILD_ROOT}/opt/eos-folly/include/double-conversion/utils.h
popd

%files
/opt/eos-folly/*

%changelog
* Wed Jan 15 2020 Mihai Patrascoiu <mihai.patrascoiu@cern.ch> - 0.0.2
- Accommodate CentOS 8 build
* Wed Nov 27 2019 Georgios Bitzes <georgios.bitzes@cern.ch> - 0.0.1
- Initial package
