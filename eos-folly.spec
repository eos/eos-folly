%global debug_package %{nil}
%define _prefix /opt/eos-folly

Name:           eos-folly
Summary:        Facebook Folly library, packaged as EOS dependency
Version:        2019.11.11.00
Release:        1%{dist}%{?_with_asan:.asan}%{?_with_tsan:.tsan}
License:        Apache
URL:            https://github.com/facebook/folly.git
Source0:        https://github.com/facebook/folly/archive/v%{version}.tar.gz
%if 0%{?rhel} >= 8 || 0%{?fedora} >= 34
#https://github.com/facebook/folly/commit/9939b376180bba62251adcce0ac1d3ae73ac9480
Patch0:         folly-limits.patch
#https://github.com/facebook/folly/commit/7df2d7e5098119c1562422ac9571e70f032adb50
Patch1:         folly-glibc.patch
#https://github.com/facebook/folly/issues/1414
Patch2:         folly-cmake.patch
%endif

# For asan/tsan builds we need devtoolset-9
%if %{?_with_asan:1}%{!?_with_asan:0} || %{?_with_tsan:1}%{!?_with_tsan:0}
%define devtoolset devtoolset-9
%else
%define devtoolset devtoolset-8
%endif

# Handle the different binaries for the cmake package depending on the OS
%if 0%{?rhel} == 7
BuildRequires: %{devtoolset}-gcc-c++
BuildRequires: cmake3
%define cmake cmake3
%else
BuildRequires: perl-Data-Dumper
BuildRequires: cmake
%define cmake cmake
%endif

BuildRequires: gcc-c++
BuildRequires: make
BuildRequires: which
BuildRequires: zlib-static
BuildRequires: zlib-devel
BuildRequires: m4
BuildRequires: automake
BuildRequires: libtool
BuildRequires: openssl
BuildRequires: openssl-devel
BuildRequires: libevent
BuildRequires: libevent-devel

BuildRequires: eos-folly-deps = %{version}
Requires: eos-folly-deps = %{version}

%if %{?_with_asan:1}%{!?_with_asan:0}
%if 0%{?rhel} == 7
BuildRequires: libasan5, %{devtoolset}-libasan-devel
Requires: libasan5
%else
BuildRequires: libasan
Requires: libasan
%endif
%endif

%if %{?_with_tsan:1}%{!?_with_tsan:0}
%if 0%{?rhel} == 7
BuildRequires: libtsan, %{devtoolset}-libtsan-devel
Requires: libtsan
%else
BuildRequires: libtsan
Requires: libtsan
%endif
%endif

%description
Facebook Folly, used as EOS build dependency.

%package devel
Summary: eos-folly development files
Group: Development/Libraries

%description devel
This package provides the headers and static library for eos-folly.

%prep
%setup -q -c -n eos-folly -a 0
%if 0%{?rhel} >= 8 || 0%{?fedora} >= 34
%patch -P 0 -p1
%patch -P 1 -p1
%patch -P 2 -p1
%endif


%build
%if %{?_with_asan:1}%{!?_with_asan:0}
export CXXFLAGS='-fsanitize=address'
%endif

%if %{?_with_tsan:1}%{!?_with_tsan:0}
export CXXFLAGS='-fsanitize=thread'
%endif

export CXXFLAGS="${CXXFLAGS} -g3 -fPIC -Wno-nonnull"

%if 0%{?rhel} == 7
source /opt/rh/%{devtoolset}/enable
%endif

# Alma 8 has a version of CMake which has a bug that folly CMakeFileLists trigger
%if 0%{?rhel} == 8
export TMP="/tmp"
echo "Installing and using cmake 3.27.4"
curl --location https://github.com/Kitware/CMake/releases/download/v3.27.4/cmake-3.27.4-Linux-x86_64.sh --output $TMP/cmake-3.27.4-Linux-x86_64.sh
chmod +x $TMP/cmake-3.27.4-Linux-x86_64.sh
mkdir -p $TMP/cmake_install/ && $TMP/cmake-3.27.4-Linux-x86_64.sh --prefix="$TMP/cmake_install/" --skip-license
%define cmake $TMP/cmake_install/bin/cmake
%endif

mkdir TEMPROOT
TEMP_ROOT=$PWD/TEMPROOT

#-------------------------------------------------------------------------------
# Compile folly
#-------------------------------------------------------------------------------
pushd folly-%{version}
mkdir builddir && cd builddir
%{cmake} --version
%{cmake} .. -DCMAKE_PREFIX_PATH=%{_prefix} -DCMAKE_INSTALL_PREFIX=%{_prefix}
make %{?_smp_mflags}
popd

%install

%if 0%{?rhel} == 7
source /opt/rh/%{devtoolset}/enable
%endif

#-------------------------------------------------------------------------------
# Install folly
#-------------------------------------------------------------------------------
pushd folly-%{version}/
cd builddir
make DESTDIR=%{buildroot} install %{?_smp_mflags}
popd

#-------------------------------------------------------------------------------
# Make folly shared library
#-------------------------------------------------------------------------------
%if %{?_with_asan:1}%{!?_with_asan:0}
export LIBASAN='-lasan'
%else
export LIBASAN=''
%endif

%if %{?_with_tsan:1}%{!?_with_tsan:0}
export LIBTSAN='-ltsan'
%else
export LIBTSAN=''
%endif

g++ -shared ${CXXFLAGS} -o libfolly.so -Wl,--whole-archive %{buildroot}%{_prefix}/lib/libfolly.a %{_prefix}/lib/libdouble-conversion.a %{_prefix}/lib/libglog.a -Wl,-no-whole-archive -Wl,-rpath,%{_prefix}/lib -L%{_prefix}/lib -lboost_context -lboost_program_options -lssl -levent -lboost_filesystem -lboost_regex -lgflags ${LIBASAN} ${LIBTSAN}
%{__install} -D -m 755 ./libfolly.so %{buildroot}%{_prefix}/lib/libfolly.so

# Avoid that rpmbuild fails due to the RPATH value set on the libfolly.so
export QA_RPATHS=$(( 0x0001|0x0002 ))

%files
%{_prefix}/*

%changelog
* Tue Oct  13 2020 Fabio Luchetti <fabio.luchetti@cern.ch> - 0.1.0
- Update to v2020.10.05.00
* Thu Apr 12 2018 Georgios Bitzes <georgios.bitzes@cern.ch> - 0.0.1
- Initial package
